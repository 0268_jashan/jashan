from django.db import models
from django.contrib.auth.models import User

import datetime
# cd = datetime.datetime.now().date

class Student(models.Model):
    c = (
        ("M","Male"),("F","Female")
    )
    name = models.CharField(max_length=250)
    email = models.EmailField(max_length=200)
    roll_no = models.IntegerField(unique=True)
    # date_of_birth = models.Datefields(blank=True,default=cd)
    fee = models.FloatField()
    gender = models.CharField(max_length=150,choices=c)
    address = models.TextField(blank=True)
    is_registered = models.BooleanField()


    def __str__(self):
         return self.name+" "+str(self.roll_no)
      
    class Meta:
        verbose_name_plural ="Student"        



class Contact_Us(models.Model):
    name =   models.CharField(max_length=250)
    email =  models.EmailField(max_length=250)
    subject = models.CharField(max_length=250)
    message = models.TextField()
    added_on = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name


    class Meta:
        verbose_name_plural = "Contact"    


class Category(models.Model):
    cat_name = models.CharField(max_length=250)
    cover_pic = models.FileField(upload_to="media/%y/%m/%d")
    description = models.TextField()
    added_on = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.cat_name

    class Meta:
        verbose_name_plural = "Category" 

class register_table(models.Model):

    c = (
        ("M","Male"),("F","Female")
    )
    user = models.OneToOneField(User,on_delete=models.CASCADE)
    contact_number = models.IntegerField()
    email = models.EmailField(max_length=250,null=True,blank=True)
    profile_pic = models.ImageField(upload_to = "profiles/%y/%m/%d",null=True,blank=True)
    age = models.CharField(max_length=250,null=True,blank=True)
    city = models.CharField(max_length=250,null=True,blank=True)
    about = models.TextField(blank=True,null=True)
    gender = models.CharField(max_length=250,default="Male")
    occupation = models.CharField(max_length=250,null=True,blank=True)
    added_on = models.DateTimeField(auto_now_add=True,null=True)
    update_on = models.DateTimeField(auto_now=True,null=True)
    
    def __str__(self):
        return self.user.username
    
    class Meta:
        verbose_name_plural = "Register Table" 

class add_course(models.Model):
    instructor = models.ForeignKey(User,on_delete = models.CASCADE) 
    course_name= models.CharField(max_length=250)
    course_category = models.ForeignKey(Category,on_delete = models.CASCADE)
    course_price = models.FloatField()
    sale_price = models.FloatField(null=True)
    course_image = models.ImageField(upload_to = "products/%y/%m/%d")
    details = models.TextField()

    def __str__(self):
        return self.instructor.username


    class Meta:
        verbose_name_plural = "Add Course" 

class cart(models.Model):
    user = models.ForeignKey(User,on_delete = models.CASCADE)
    course = models.ForeignKey(add_course,on_delete = models.CASCADE)
    quantity = models.IntegerField()
    status = models.BooleanField(default=False)
    added_on = models.DateTimeField(auto_now_add=True,null=True)
    update_on = models.DateTimeField(auto_now=True,null=True)

    def __str__(self):
        return self.user.username

    class Meta:
        verbose_name_plural = "Cart"

class order(models.Model):
    inst_id = models.ForeignKey(User,on_delete=models.CASCADE,null=True)
    cart_ids = models.CharField(max_length=250)
    course_ids = models.CharField(max_length=250)
    invoice_id = models.CharField(max_length=250,null=True)
    status = models.BooleanField(default=False)
    processed_on = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.inst_id.username

    class Meta:
        verbose_name_plural = "Order"