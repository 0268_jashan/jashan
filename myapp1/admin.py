from django.contrib import admin
from myapp1.models import Student,Contact_Us,Category,register_table,add_course,cart,order

admin.site.site_header="My Website"

class StudentAdmin(admin.ModelAdmin):
    list_display = ["name","roll_no","email","fee","gender","address","is_registered"]
    search_fields = ["roll_no","name"]
    list_filter = ["name","gender"]
    list_editable = ["email","roll_no","address"]
    # fields = ["name","email","roll_no"]

class Contact_UsAdmin(admin.ModelAdmin):
    list_display=["id","name","email","subject","message","added_on"]
    list_filter = ["added_on","name"]
    search_fields = ["name"]
    list_editable = ["message","subject"]
    fields = ["Email","name","subject","message"] 

class CategoryAdmin(admin.ModelAdmin):
    list_display = ["id","cat_name","description","added_on"]     

   

admin.site.register(Student,StudentAdmin)
admin.site.register(Contact_Us,Contact_UsAdmin)
admin.site.register(Category,CategoryAdmin)
admin.site.register(register_table)
admin.site.register(add_course)
admin.site.register(cart)
admin.site.register(order)