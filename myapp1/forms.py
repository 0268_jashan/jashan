from django import forms
from myapp1.models import add_course

class add_course_form(forms.ModelForm):
    class Meta:
        model = add_course
        # exclude = ["course_name"]
        fields = ["course_name","course_category","course_price","sale_price","course_image","details"]