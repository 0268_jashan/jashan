from django.shortcuts import render, get_object_or_404,reverse
from django.http import HttpResponse,HttpResponseRedirect,JsonResponse
from myapp1.models import Contact_Us,Category,register_table,add_course,cart,order
from django.contrib.auth.models import User
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.decorators import login_required
from myapp1.forms import add_course_form
from django.db.models import Q
from datetime import datetime
from django.core.mail import EmailMessage

def home(request):
    if "abcd_id" in request.COOKIES:
        uid = request.COOKIES["abcd_id"]
        usr = get_object_or_404(User,id=uid)
        login(request,usr)
        if usr.is_superuser:
           return HttpResponseRedirect("/admin")
        if usr.is_active:
           return HttpResponseRedirect("/instprofile/")

    recent = Contact_Us.objects.all().order_by("-id")[:5]
    cats = Category.objects.all().order_by("cat_name")

    return render(request,"home.html",{"message":recent,"category":cats})

def allcourses(request):
    context = {}
    allcourses = add_course.objects.all().order_by("course_name")
    context["course"] = allcourses

    if "qry" in request.GET:
        q = request.GET["qry"]
        course = add_course.objects.filter(course_name__icontains=q)
        # course = add_course.objects.filter(course_name__icontains=q)|
        context["course"] = course
        context["abcd"] = "search"

    return render(request,"allcourses.html",context)

def courses(request):
    context = {}
    ch = register_table.objects.filter(user__id=request.user.id)
    if len(ch)>0:
        data = register_table.objects.get(user__id=request.user.id)
        context["data"] = data
    courses = add_course.objects.all().order_by("course_name")
    context["course"] = courses
    # data = register_table.objects.get(user__id=request.user.id)
    return render(request,"course.html",context)


@login_required
def instprofile(request):
    context = {}
    check = register_table.objects.filter(user__id=request.user.id)
    if len(check)>0:
        data = register_table.objects.get(user__id=request.user.id)
        context["data"] = data
    return render(request,"inst_profile.html",context)    

def contacts(request):
    all_data = Contact_Us.objects.all().order_by("-id")
    if request.method=="POST":
        nm = request.POST["name"]
        email = request.POST["email"]
        sub = request.POST["subject"]
        msz = request.POST["message"]
        # return HttpResponse("<h1>"+nm+email+sub+msz+"</h1>")

        data = Contact_Us(name=nm,email=email,subject=sub,message=msz)
        data.save()
        # res = "Dear {} Thanks for your Feedback".format(nm)
        # return render(request,"contact.html",{"Status":res,"message":all_data})
        # return HttpResponse("<h1 style='color:green;'>Dear {} Data Saved Successfully!</h1>".format(nm))
    

    return render(request,"contact.html",{"message":all_data})   

def resetpass(request):
    context={}
    ch = register_table.objects.filter(user__id=request.user.id)
    if len(ch)>0:

        data = register_table.objects.get(user__id=request.user.id)
        context["data"] = data
    if request.method=="POST":
        old = request.POST["oldpass"]
        new_pass = request.POST["newpass"]
        
        user = User.objects.get(id=request.user.id)
        un = user.username
        check = user.check_password(old)
        if check==True:
            user.set_password(new_pass)
            user.save()
            context["msz"] = "Password Changed Successfully"
            context["col"] = "alert-success"
            user = User.objects.get(username=un)
            login(request,user)

        else:
            context["msz"] = "Incorrect Current Password"
            context["col"] = "alert-danger"

    return render(request,"reset.html",context) 

def about(request):
    return render(request,"Aboout.html")    

def edit_profile(request):
    context = {}
    check = register_table.objects.filter(user__id=request.user.id)
    if len(check)>0:
        data = register_table.objects.get(user__id=request.user.id)
        context["data"]=data
    if request.method=="POST":
        print(request.FILES)
        fn = request.POST["fname"]
        ln = request.POST["lname"]
        em = request.POST["email"]
        con = request.POST["phone"]
        age = request.POST["age"]
        ct = request.POST["city"]
        gen = request.POST["gender"]
        occ = request.POST["occ"]
        abt = request.POST["about"]


        usr = User.objects.get(id=request.user.id)
        usr.first_name = fn
        usr.last_name = ln
        usr.email = em
        usr.save()

        data.contact_number = con
        data.age = age
        data.city = ct
        data.gender = gen
        data.occupation = occ
        data.about = abt
        data.save()

        if "image" in request.FILES:
            img = request.FILES["image"]
            data.profile_pic = img
            data.save()
        
        context["status"] = "Changes Saved Successfully!!!"

    return render(request,"edit_profile.html",context) 

def view_profile(request):
    data = register_table.objects.get(user__id=request.user.id)
    return render(request,"view_profile.html",{"data":data})    

def registration(request):
    if "abcd_id" in request.COOKIES:
        uid = request.COOKIES["abcd_id"]
        usr = get_object_or_404(User,id=uid)
        login(request,usr)
        if usr.is_superuser:
           return HttpResponseRedirect("/admin")
        if usr.is_active:
           return HttpResponseRedirect("/instprofile/")
    if request.method=="POST":
        fname = request.POST["first"]
        lname = request.POST["last"]
        uname = request.POST["uname"]
        email = request.POST["email"]
        con = request.POST["contact"]
        pwd = request.POST["password1"]
        cpwd = request.POST["password2"]
        gen = request.POST["gender"]
        tp = request.POST["utype"]
        
        usr = User.objects.create_user(uname,email,pwd)
        usr.first_name = fname
        usr.last_name = lname
        if tp=="inst":
            usr.is_staff = True
        usr.save()

        reg = register_table(user=usr, contact_number=con,gender=gen)
        reg.save()
        
    return render(request,"regis.html")
    # ,{"status":"Mr./Miss. {} Account Created Successfully".format(fname)})

def check_user(request):
    if request.method=="GET":
        un = request.GET["usern"]
        check = User.objects.filter(username=un) 
        if len(check) == 1:
            return HttpResponse("Exists")
        else:
            return HttpResponse("Not Exists")          

def log_in(request):
    if request.method=="POST":
        un = request.POST["username"]
        pwd = request.POST["password"]

        user = authenticate(username=un,password=pwd)
        if user:
            login(request,user)
            if user.is_superuser:
                return HttpResponseRedirect("/admin")
            else:
                res =  HttpResponseRedirect("/instprofile/")
                if "rememberme" in request.POST:
                    res.set_cookie("abcd_id",user.id)
                    res.set_cookie("date_login",datetime.now())
                return res
            # if user.is_staff:
            #     return HttpResponseRedirect("instprofile")        
        else:
            return render(request,"log_in.html",{"status":"Invalid Username or Password"})    
    # return HttpResponse("Called")
    return render(request,"log_in.html")   
@login_required
def stuprofile(request):
    data = register_table.objects.get(user__id=request.user.id)
    return render(request,"studentprofile.html",{"data":data})  

def wishlist(request):
    data = register_table.objects.get(user__id=request.user.id)
    return render(request,"wish_list.html",{"data":data}) 

def singlecourse(request):
    context = {}
    ch = register_table.objects.filter(user__id=request.user.id)
    if len(ch)>0:
        data = register_table.objects.get(user__id=request.user.id)
        context["data"] = data
    id = request.GET["cid"]
    obj = add_course.objects.get(id=id)
    context["course"] = obj
    return render(request,"single_course.html",context) 
@login_required
def user_logout(request):
    logout(request)
    res =  HttpResponseRedirect("/") 
    res.delete_cookie("abcd_id")
    res.delete_cookie("date_login")
    return res                              

def add_course_view(request):
    context={}
    ch = register_table.objects.filter(user__id=request.user.id)
    if len(ch)>0:
        data = register_table.objects.get(user__id=request.user.id)
        context["data"] = data
    form = add_course_form()
    if request.method=="POST":
        form = add_course_form(request.POST,request.FILES)
        if form.is_valid():
            data = form.save(commit=False)
            login_user = User.objects.get(username=request.user.username)
            data.instructor = login_user
            data.save()
            context["status"] = "{} Added Successfully".format(data.course_name)

    context["form"] = form    


    return render(request,"add_course.html",context)

def mycourse(request):
    context = {}
    ch = register_table.objects.filter(user__id=request.user.id)
    if len(ch)>0:
        data = register_table.objects.get(user__id=request.user.id)
        context["data"] = data
    all = add_course.objects.filter(instructor__id=request.user.id).order_by("-id")
    context["course"] = all
    return render(request,"mycourse.html",context)

# def single_course(request):
#     pass

def update_course(request):
    context = {}
    ch = register_table.objects.filter(user__id=request.user.id)
    if len(ch)>0:
        data = register_table.objects.get(user__id=request.user.id)
        context["data"] = data
    cats = Category.objects.all().order_by("cat_name")
    context["category"] = cats


    id = request.GET["cid"]
    course = get_object_or_404(add_course,id=id)
    context["course"] = course

    if request.method=="POST":
        cn = request.POST["cname"]
        ct_id = request.POST["ccat"]
        cr = request.POST["cp"]
        sp = request.POST["sp"]
        des = request.POST["des"]
       
        cat_obj = Category.objects.get(id=ct_id)

        course.course_name = cn
        course.course_category = cat_obj
        course.course_price = cr
        course.sale_price = sp
        course.details = des
        if "cimg" in request.FILES:
            img = request.FILES["cimg"]
            course.course_image = img
        course.save()
        context["status"] = "Changes Saves Successfully"
        context["id"] = id
    return render(request,"update_course.html",context)

def delete_course(request):
    context = {}
    if "cid" in request.GET:
        cid = request.GET["cid"]
        course = get_object_or_404(add_course,id=cid)
        context["course"] = course

        if "action" in request.GET:
            course.delete()
            context["status"] = str(course.course_name)+"Removed Succesfully!!"

    return render(request,"delete_course.html",context)
      
def sendemail(request):
    context = {}
    ch = register_table.objects.filter(user__id=request.user.id)
    if len(ch)>0:
        data = register_table.objects.get(user__id=request.user.id)
        context["data"] = data
    if request.method=="POST":
        rec = request.POST["to"].split(",")
        sub = request.POST["sub"]
        msz = request.POST["msz"]
        try:
            em = EmailMessage(sub,msz,to=rec)    
            em.send()
            context["message"] = "Email Sent"
            context["cls"] = "alert-success"
        except:
            context["message"] = "Could not sent, Please check Internet Connection/Email Address"
            context["cls"] = "alert-danger"
    return render(request,"sendemail.html",context)  

def forget_password(request):
    context = {}
    if request.method=="POST":
        un = request.POST["username"]
        pwd = request.POST["npass"]

        user = get_object_or_404(User,username=un)
        user.set_password(pwd)
        user.save()

        login(request,user)
        if user.is_superuser:
            return HttpResponseRedirect("/admin")
        else:
            return HttpResponseRedirect("/instprofile/")    
        # context["status"] = "Password Changed Successfully!!"


    return render(request,"forget_password.html",context) 

import random

def reset_pass(request):
    un = request.GET["username"]
    try:
        user = get_object_or_404(User,username=un)
        otp = random.randint(1000,9999)
        msz = "Dear {} \n{} is your one time Password (OTP) \nDo not share it with others \nThanks&Regards \nPioneerPython".format(user.username, otp)
        try:
            email = EmailMessage("Account Verification",msz,to=[user.email])
            email.send()
            return JsonResponse({"status":"sent","email":user.email,"rotp":otp})
        except:
            return JsonResponse({"status":"error","email":user.email})
    except:
        return JsonResponse({"status":"failed"})

            
def add_to_cart(request):
    context = {}
    items = cart.objects.filter(user__id=request.user.id,status=False)
    context["items"] = items
    if request.user.is_authenticated:
        if request.method=="POST":
            cid = request.POST["cid"]
            qty = request.POST["qty"]
            is_exist = cart.objects.filter(course__id=cid,user__id=request.user.id,status=False)
            if len(is_exist)>0:
                context["msz"] = "Item Already Exists in Your Cart"
                context["cls"] = "alert alert-warning"
            else:
                course = get_object_or_404(add_course,id=cid)
                usr = get_object_or_404(User,id = request.user.id)
                c = cart(user=usr,course=course,quantity=qty)
                c.save()
                context["msz"] = "{} Added in Your Cart".format(course.course_name)
                context["cls"] = "alert alert-success"
    else:
        context["status"] = "Please Login First To View Your Cart"
    return render(request,"cart.html",context)

def get_cart_data(request):
    items = cart.objects.filter(user__id=request.user.id, status=False)
    sale,total,quantity=0,0,0
    for i in items:
        sale += int(i.course.sale_price)*i.quantity
        total += int(i.course.course_price)*i.quantity
        quantity += int(i.quantity)

    res = {
        "total":total,"offer":sale,"qun":quantity
    }    
    return JsonResponse(res)

def change_quan(request):
    if "quantity" in request.GET:
        cid = request.GET["cid"]
        qty = request.GET["quantity"]
        cart_obj = get_object_or_404(cart,id=cid)
        cart_obj.quantity = qty
        cart_obj.save()
        return HttpResponse(cart_obj.quantity)

    if "delete_cart" in request.GET:
        id = request.GET["delete_cart"]
        cart_obj = get_object_or_404(cart,id=id)
        cart_obj.delete()
        return HttpResponse(1)

from paypal.standard.forms import PayPalPaymentsForm
from django.conf import settings

def process_payment(request):
    items = cart.objects.filter(user_id__id=request.user.id,status=False)
    courses=""
    amt=0
    inv = "INV-"
    cart_ids = ""
    c_ids = ""
    for j in items:
        courses += str(j.course.course_name)+"\n"
        c_ids += str(j.course.id)+","
        amt += float(j.course.sale_price)
        inv += str(j.id)
        cart_ids += str(j.id)+","
        
    paypal_dict = {
        'business': settings.PAYPAL_RECEIVER_EMAIL,
        'amount': str(amt),
        'item_name': courses,
        'invoice': inv,
        'notify_url': 'http://{}{}'.format("127.0.0.1:8000",
                                           reverse('paypal-ipn')),
        'return_url': 'http://{}{}'.format("127.0.0.1:8000",
                                           reverse('payment_success')),
        'cancel_return': 'http://{}{}'.format("127.0.0.1:8000",
                                              reverse('payment_failed')),                                  
    }
    usr = User.objects.get(username=request.user.username)
    ord = order(inst_id=usr,cart_ids=cart_ids,course_ids=c_ids)
    ord.save()
    ord.invoice_id = str(ord.id)+inv
    ord.save()
    request.session["order_id"] = ord.id

    form = PayPalPaymentsForm(initial=paypal_dict)
    return render(request, 'process_payment.html', {'form': form})        

def payment_success(request):
    if "order_id" in request.session:
        order_id = request.session["order_id"]
        ord_obj = get_object_or_404(order,id=order_id)
        ord_obj.status=True
        ord_obj.save()

        for i in ord_obj.cart_ids.split(",")[:-1]:
            cart_object = cart.objects.get(id=i)
            cart_object.status=True
            cart_object.save()
    return render(request,"payment_success.html")


def payment_failed(request):
    return render(request,"payment_failed.html")


def order_history(request):
    context = {}
    ch = register_table.objects.filter(user__id=request.user.id)
    if len(ch)>0:
        data = register_table.objects.get(user__id=request.user.id)
        context["data"] = data

    all_orders = [] 
    orders = order.objects.filter(inst_id__id=request.user.id).order_by("-id")
    for ord in orders:
        courses = []
        for id in ord.course_ids.split(",")[:-1]:
            cou = get_object_or_404(add_course, id=id)
            courses.append(cou)
        odr = {
            "order_id":ord.id,
            "courses":courses,
            "invoice":ord.invoice_id,
            "status":ord.status,
            "date":ord.processed_on,
        }    
        all_orders.append(odr)
    context["order_history"] = all_orders

    return render(request,"order_history.html",context)

def my_student(request):
    context = {}
    ch = register_table.objects.filter(user__id=request.user.id)
    if len(ch)>0:
        data = register_table.objects.get(user__id=request.user.id)
        context["data"] = data

    courses = cart.objects.filter(course__instructor__id=request.user.id, status=True)
    inst =[]
    ids = []
    context["times"] = len(courses)
    for i in courses:
        us = {
            "username":i.user.username,
            "first_name":i.user.first_name,
            "last_name":i.user.last_name,
            "email":i.user.email,
            "join":i.user.date_joined,
        }
        check = register_table.objects.filter(user__id=i.user.id)
        if len(check)>0:
            prf = get_object_or_404(register_table, user__id=i.user.id)
            us["profile_pic"] = prf.profile_pic
            us["contact"] = prf.contact_number
        ids.append(i.user.id)
        count = ids.count(i.user.id)

        if count<2:
            inst.append(us)
            count +=1

    context["instructors"] = inst        

    return render(request,"my_student.html",context)