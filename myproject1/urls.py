"""myproject1 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path,include
from myapp1 import views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('admin/', admin.site.urls),
    path('',views.home,name='home'),
    path('allcourses/',views.allcourses,name='allcourses'),
    path('courses',views.courses,name='courses'),
    path('instprofile/',views.instprofile,name='instpro'),
    path('contacts',views.contacts,name='cont'),
    path('reset',views.resetpass,name='reset'),
    path('about',views.about,name="About"),
    path('edit_profile',views.edit_profile,name='edit_profile'),
    path('regis',views.registration,name='registration'),

    path('check_user/',views.check_user,name='check_user'),
    path('log_in',views.log_in,name='log_in'),
    path('user_logout',views.user_logout,name='user_logout'),
    path('student/',views.stuprofile,name='stuprofile'),
    path('wishlist',views.wishlist,name="wishlist"),
    path('single',views.singlecourse,name='single'),
    path('view_profile',views.view_profile,name='view_profile'),
    path('add_course',views.add_course_view,name='add_course'),
    path('mycourse',views.mycourse,name='mycourse'),

    # path('single_course',views.single_course,name='single_course'),
    path('update_course',views.update_course,name='update_course'),
    path('delete_course',views.delete_course,name='delete_course'),
    path('sendemail',views.sendemail,name='sendemail'),
    path('forget_password',views.forget_password,name='forget_password'),
    path('reset_pass',views.reset_pass,name='reset_pass'),
    path('cart',views.add_to_cart,name='cart'),

    path('get_cart_data',views.get_cart_data,name='get_cart_data'),
    path('change_quan',views.change_quan,name='change_quan'),
    path('process_payment',views.process_payment,name='process_payment'),
    path('payment_success',views.payment_success,name='payment_success'),
    path('payment_failed',views.payment_failed,name='payment_failed'),
    path('order_history',views.order_history,name='order_history'),
    path('my_student',views.my_student,name='my_student'),
    
    

    path('paypal/', include('paypal.standard.ipn.urls')),

]+static(settings.MEDIA_URL,document_root = settings.MEDIA_ROOT)
